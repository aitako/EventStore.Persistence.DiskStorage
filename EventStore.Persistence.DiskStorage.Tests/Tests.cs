﻿using System;
using System.IO;
using System.Linq;
using EventStore.Dispatcher;
using NUnit.Framework;

namespace EventStore.Persistence.DiskStorage.Tests
{
    [TestFixture]
    public class Tests
    {
        [TearDown]
        public void Teardown()
        {
            Directory.Delete(_Directory, true);
        }

        private string _Directory = @"c:\EventStore.Persistence.DiskStorage.Tests\";

        private static Guid LastCommit;

        private IStoreEvents _Store;

        [SetUp]
        [Test]
        public void CanBuildStore()
        {
            Directory.CreateDirectory(_Directory);

            _Store = Wireup.Init()
                           .UsingDiskStoragePersistence(_Directory)
                           .InitializeStorageEngine()
                           .UsingSynchronousDispatchScheduler(
                               new DelegateMessageDispatcher((commit) => { LastCommit = commit.CommitId; }))
                           .Build();
        }

        [Test]
        public void CanCommitEventToStream()
        {
            Guid streamId = Guid.NewGuid();
            Guid commitId = Guid.NewGuid();

            IEventStream stream = _Store.CreateStream(streamId);
            var @event = new EventMessage {Body = new TestMessage("Hello world")};
            stream.Add(@event);
            stream.CommitChanges(commitId);

            Assert.That(stream.CommittedEvents.Count, Is.EqualTo(1));
            Assert.That(stream.CommittedEvents.First(), Is.EqualTo(@event));
        }

        [Test]
        public void CanCreateStream()
        {
            Guid streamId = Guid.NewGuid();
            _Store.CreateStream(streamId);
        }

        [Test]
        public void CanPurgeStore()
        {
            Guid streamId = Guid.NewGuid();
            Guid commitId = Guid.NewGuid();

            IEventStream stream = _Store.CreateStream(streamId);
            var @event = new EventMessage {Body = new TestMessage("Hello world")};
            stream.Add(@event);
            stream.CommitChanges(commitId);

            _Store.Advanced.Purge();
            IEventStream openedStream = _Store.OpenStream(streamId, 0, int.MaxValue);

            Assert.That(openedStream.CommittedEvents.Count, Is.EqualTo(0));
        }

        [Test]
        public void DispatchEvents()
        {
            Guid streamId = Guid.NewGuid();
            Guid commitId = Guid.NewGuid();

            IEventStream stream = _Store.CreateStream(streamId);
            var @event = new EventMessage {Body = new TestMessage("Hello world")};
            stream.Add(@event);
            stream.CommitChanges(commitId);

            Assert.That(LastCommit, Is.EqualTo(commitId));
        }
    }
}