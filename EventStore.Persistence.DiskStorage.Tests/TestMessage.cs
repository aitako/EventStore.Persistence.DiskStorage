﻿using System;

namespace EventStore.Persistence.DiskStorage.Tests
{
    internal class TestMessage
    {
        public TestMessage(string myString)
        {
            Id = Guid.NewGuid();
            MyString = myString;
        }

        public string MyString { get; set; }
        public Guid Id { get; set; }
    }
}