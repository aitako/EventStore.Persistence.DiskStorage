﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace EventStore.Persistence.DiskStorage.Tests
{
    [TestFixture]
    public class PerformanceTests
    {
        [SetUp]
        public void Setup()
        {
            string directory = @"c:\EventStore.Persistence.DiskStorage.Tests.Performance";

            Directory.CreateDirectory(directory);

            _Store = Wireup.Init()
                           .UsingDiskStoragePersistence(directory)
                           .InitializeStorageEngine()
                           .Build();
        }

        [TearDown]
        public void Teardown()
        {
            string directory = @"c:\EventStore.Persistence.DiskStorage.Tests.Performance";
            Directory.Delete(directory, true);
        }

        private IStoreEvents _Store;

        public void CommitNStreamsWithYEvents(int streamCount, int eventCount)
        {
            double totalMilliseconds = 0;

            var events = new List<EventMessage>();
            for (int i = 0; i < eventCount; i++)
            {
                events.Add(new EventMessage {Body = new TestMessage("Hello world")});
            }

            for (int s = 0; s < streamCount; s++)
            {
                Guid streamId = Guid.NewGuid();
                using (IEventStream stream = _Store.CreateStream(streamId))
                {
                    for (int i = 0; i < eventCount; i++)
                    {
                        stream.Add(events[i]);
                    }

                    DateTime timestamp = DateTime.Now;
                    stream.CommitChanges(Guid.NewGuid());
                    totalMilliseconds += DateTime.Now.Subtract(timestamp).TotalMilliseconds;
                }
            }
            Console.WriteLine("took {0:#,###} milliseconds", totalMilliseconds);
        }

        public void CommitNRecords(int n)
        {
            Guid streamId = Guid.NewGuid();

            var events = new List<EventMessage>();
            for (int i = 0; i < n; i++)
            {
                events.Add(new EventMessage {Body = new TestMessage("Hello world")});
            }

            using (IEventStream stream = _Store.CreateStream(streamId))
            {
                for (int i = 0; i < n; i++)
                {
                    stream.Add(events[i]);
                }

                Console.WriteLine("committing {0:#,###} records...", n);
                DateTime timestamp = DateTime.Now;
                stream.CommitChanges(Guid.NewGuid());
                Console.WriteLine("took {0:#,###} milliseconds", DateTime.Now.Subtract(timestamp).TotalMilliseconds);
            }
        }

        [Test]
        public void Commit10000Records()
        {
            CommitNRecords(10000);
        }

        [Test]
        public void Commit1000Records()
        {
            CommitNRecords(1000);
        }

        [Test]
        public void Commit100Records()
        {
            CommitNRecords(100);
        }

        [Test]
        public void Commit100StreamsWith100Records()
        {
            CommitNStreamsWithYEvents(100, 100);
        }

        [Test]
        public void Commit10Records()
        {
            CommitNRecords(10);
        }

        [Test]
        public void Commit10StreamsWith10Records()
        {
            CommitNStreamsWithYEvents(10, 10);
        }

        [Test]
        public void CommitSingleRecord()
        {
            CommitNRecords(1);
        }
    }
}