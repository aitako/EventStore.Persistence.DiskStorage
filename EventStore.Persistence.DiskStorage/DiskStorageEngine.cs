﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ServiceStack.Text;

namespace EventStore.Persistence.DiskStorage
{
    /// <summary>
    ///     Code in here is far from perfect, started as a POC, busy refactoring and cleaning up as the POC worked quite well.
    /// </summary>
    public class DiskStorageEngine : IPersistStreams
    {
        private const string EVENT_FOLDER_PATH_TEMPLATE = "{0}\\events";
        private const string UNDISPATCHED_FOLDER_PATH_TEMPLATE = "{0}\\undispatched";
        private const string DISPATCHED_FOLDER_PATH_TEMPLATE = "{0}\\dispatched";

        // ugly, need to clean this up a bit.
        private const string EVENT_PATH_TEMPLATE = "{0}\\events\\{1}.json";
        private const string UNDISPATCHED_PATH_TEMPLATE = "{0}\\undispatched\\{1}.json";
        private const string DISPATCHED_PATH_TEMPLATE = "{0}\\dispatched\\{1}.json";

        public DiskStorageEngine(string dataPath)
        {
            DataPath = dataPath;
        }

        private string DataPath { get; set; }

        public IEnumerable<Commit> GetFrom(DateTime start)
        {
            throw new Exception("This function has not been implemented");
        }

        public IEnumerable<Commit> GetUndispatchedCommits()
        {
            var undispatchedCommits = new List<Commit>();

            string undispatchedFolder = string.Format(UNDISPATCHED_FOLDER_PATH_TEMPLATE, DataPath);
            string[] files = Directory.GetFiles(undispatchedFolder, "*.json");
            foreach (string file in files)
            {
                using (StreamReader reader = CreateJsonFileReader(file))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        var commit = JsonSerializer.DeserializeFromString<Commit>(line);
                        undispatchedCommits.Add(commit);
                    }
                }
            }
            return undispatchedCommits.AsEnumerable();
        }

        public void Initialize()
        {
            WriteTraceMessage("Initialising disk storage.");

            CreateFolderIfNotExists(string.Format(DataPath, DataPath));
            CreateFolderIfNotExists(string.Format(EVENT_FOLDER_PATH_TEMPLATE, DataPath));
            CreateFolderIfNotExists(string.Format(UNDISPATCHED_FOLDER_PATH_TEMPLATE, DataPath));
            CreateFolderIfNotExists(string.Format(DISPATCHED_FOLDER_PATH_TEMPLATE, DataPath));
        }

        public void MarkCommitAsDispatched(Commit commit)
        {
            string fileName = string.Format(UNDISPATCHED_PATH_TEMPLATE, DataPath, commit.CommitId);
            string destination = string.Format(DISPATCHED_PATH_TEMPLATE, DataPath, commit.CommitId);
            File.Move(fileName, destination);
        }

        public void Purge()
        {
            foreach (string file in Directory.GetFiles(string.Format(EVENT_FOLDER_PATH_TEMPLATE, DataPath)))
            {
                File.Delete(file);
            }
        }

        public void Dispose()
        {
        }

        public void Commit(Commit attempt)
        {
            DateTime timestamp = DateTime.UtcNow;
            string commitString = attempt.ToJson();

            // create/append event store file
            string fileName = string.Format(EVENT_PATH_TEMPLATE, DataPath, GetEventStoreFileName(attempt.StreamId));
            using (StreamWriter writer = CreateJsonFileWriter(fileName))
            {
                writer.WriteLine(commitString);
            }

            // create dispatch file
            string undispatchedCommitFileName = string.Format(UNDISPATCHED_PATH_TEMPLATE, DataPath, attempt.CommitId);
            using (StreamWriter writer = CreateJsonFileWriter(undispatchedCommitFileName))
            {
                writer.WriteLine(commitString);
            }
        }

        public IEnumerable<Commit> GetFrom(Guid streamId, int minRevision, int maxRevision)
        {
            var commits = new List<Commit>();
            string fileName = string.Format(EVENT_PATH_TEMPLATE, DataPath, GetEventStoreFileName(streamId));
            if (File.Exists(fileName))
            {
                using (StreamReader reader = CreateJsonFileReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        var commit = JsonSerializer.DeserializeFromString<Commit>(line);
                        if (commit.StreamRevision >= minRevision && commit.StreamRevision <= maxRevision)
                        {
                            commits.Add(commit);
                        }
                    }
                }
            }

            return commits.AsEnumerable();
        }

        public bool AddSnapshot(Snapshot snapshot)
        {
            throw new Exception("This function will not be implemented for the moment.");
        }

        public Snapshot GetSnapshot(Guid streamId, int maxRevision)
        {
            throw new Exception("This function will not be implemented for the moment.");
        }

        public IEnumerable<StreamHead> GetStreamsToSnapshot(int maxThreshold)
        {
            throw new Exception("This function will not be implemented for the moment.");
        }

        private string GetEventStoreFileName(Guid streamId)
        {
            return streamId.ToString();
        }

        private void WriteTraceMessage(string message, params string[] parameters)
        {
            // change this to use Trace for production;
            Console.WriteLine(string.Format("[DiskStorage] {0}", string.Format(message, parameters)));
        }

        private void CreateFolderIfNotExists(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                WriteTraceMessage("creating path at : {0}", folderPath);
                Directory.CreateDirectory(folderPath);
            }
        }

        private StreamWriter CreateJsonFileWriter(string path)
        {
            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }

            FileStream stream = File.Open(path, FileMode.Append, FileAccess.Write, FileShare.None);
            var writer = new StreamWriter(stream);
            return writer;
        }

        private StreamReader CreateJsonFileReader(string path)
        {
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.None);
            var reader = new StreamReader(stream);
            return reader;
        }
    }
}