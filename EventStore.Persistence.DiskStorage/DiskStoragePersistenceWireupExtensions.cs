﻿namespace EventStore.Persistence.DiskStorage
{
    public static class DiskStoragePersistenceWireupExtensions
    {
        public static PersistenceWireup UsingDiskStoragePersistence(
            this Wireup wireup, string dataPath)
        {
            return new DiskStorageWireup(wireup, dataPath);
        }

        public static PersistenceWireup UsingDiskStoragePersistence(
            this Wireup wireup)
        {
            return new DiskStorageWireup(wireup);
        }
    }
}