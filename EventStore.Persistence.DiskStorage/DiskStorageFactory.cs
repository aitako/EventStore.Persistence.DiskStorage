﻿using System.IO;
using System.Reflection;

namespace EventStore.Persistence.DiskStorage
{
    public class DiskStorageFactory : IPersistenceFactory
    {
        public DiskStorageFactory(string dataPath)
        {
            DataPath = dataPath;
        }

        public DiskStorageFactory()
            : this(string.Format("{0}\\EventStore", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)))
        {
        }

        public string DataPath { get; set; }

        public IPersistStreams Build()
        {
            return new DiskStorageEngine(DataPath);
        }
    }
}