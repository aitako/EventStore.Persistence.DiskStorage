﻿namespace EventStore.Persistence.DiskStorage
{
    public class DiskStorageWireup : PersistenceWireup
    {
        public DiskStorageWireup(Wireup inner, string rootPath)
            : base(inner)
        {
            Container.Register(c => new DiskStorageFactory(rootPath).Build());
        }

        public DiskStorageWireup(Wireup inner)
            : base(inner)
        {
            Container.Register(c => new DiskStorageFactory().Build());
        }
    }
}