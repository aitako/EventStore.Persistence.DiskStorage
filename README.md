EventStore.Persistence.DiskStorage
==================================

A disk storage persistence plugin for EventStore.

Files are stored under a folder structure as .json files. The event dispatch queue is 
managed by moving files from the undispatched to dispatched folders. Commits can be 
re-dispatched by copying the commits in the dispatched folder to the undispatched folder.

            var _Store = Wireup.Init()
             .UsingDiskStoragePersistence([OPTIONAL PATH FOR DATA FOLDER])
             .InitializeStorageEngine()
             .Build();

Simple as that. 